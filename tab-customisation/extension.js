const vscode = require('vscode');
const crypto = require('crypto')
var fs = require("fs");
var path = require("path");
var appDir = path.dirname(require.main.filename);
var base = path.join(appDir, "vs", "code");
var htmlFile = path.join(base, "electron-browser", "workbench", "workbench.html");
const rootDir = path.join(appDir, '..')
const productFile = path.join(rootDir, 'product.json')
/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
	var html = fs.readFileSync(htmlFile, "utf-8");
	html = html.replace(/(\r\n|\r|\n){2,}/g, '$1\n');
	var CustomCodeStart = "<!-- The Arcane Brony's Custom Code -->";
	var CustomCode = `${CustomCodeStart}
<style>
  /* Corner Image */
  body>.monaco-workbench>.monaco-grid-view>.monaco-grid-branch-node>.monaco-split-view2>.monaco-scrollable-element>.split-view-container::after {background-image: url('https://i.pinimg.com/originals/3d/21/ff/3d21ff1b0eb0c4e1bdf6df87ed2f2c74.png');content:'';pointer-events:none;position:absolute;z-index:9001;width: 100%;height:100%;background-position:calc(100%) calc(100% - 22px);background-size:20%;background-repeat:no-repeat;opacity:1;}

  /* Background Image */
  .monaco-workbench .part.editor > .content {
    background-image: url('') !important;
    background-position: center;
    background-size: cover;
    content:'';
    z-index:9001;
    width:100%;
    height:100%;
    background-repeat:no-repeat;
    opacity:1;
  }

  /* Notifications fix */
  .monaco-workbench>.notifications-center.visible, 
  .monaco-workbench>.notifications-toasts.visible {
    z-index: 10000; 
  }
</style>
`
	try {
		fs.writeFileSync(htmlFile, html.split(CustomCodeStart)[0] + CustomCode);
	} catch (error) {
		vscode.window.showInformationMessage("Failed to inject into VSCode, are you running as admin?")
		console.log(error);
	}
	fixChecksums();
	new StatusBar().initialize();
	vscode.window.showInformationMessage('Customisations for The Arcane Brony has loaded!');
	let disposable = vscode.commands.registerCommand('tab-customisation.helloWorld', function () {
		vscode.window.showInformationMessage('Customisations for The Arcane Brony has loaded!');
	});

	context.subscriptions.push(disposable);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() { }

module.exports = {
	activate,
	deactivate
}
function fixChecksums() {
	const product = require(productFile)
	let changed = false
	for (const [filePath, curChecksum] of Object.entries(product.checksums)) {
		const checksum = computeChecksum(path.join(appDir, ...filePath.split('/')))
		if (checksum !== curChecksum) {
			product.checksums[filePath] = checksum
			changed = true
		}
	}
	if (changed) {
		const json = JSON.stringify(product, null, '\t')
		try {
			fs.writeFileSync(productFile, json, { encoding: 'utf8' })
		} catch (err) {
			vscode.window.showInformationMessage("Something went wrong whilst fixing checksums. You will see an \"Installation corrupted\" warning. This is safe to ignore!");
			console.error(err)
		}
	}
}
function computeChecksum(file) {
	var contents = fs.readFileSync(file)
	return crypto
		.createHash('md5')
		.update(contents)
		.digest('base64')
		.replace(/=+$/, '')
}
class StatusBar {
	constructor() {
		this._statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, Number.MIN_SAFE_INTEGER // furthest right on the right
		);
		this._statusBarItem.show();
	}
	initialize() {
		this.setText("awoo");
	}
	dispose() {
		this._statusBarItem.dispose();
	}
	setText(text) {
		this._statusBarItem.text = `${text} $(heart)`;
	}
}
exports.StatusBarComponent = new StatusBar();